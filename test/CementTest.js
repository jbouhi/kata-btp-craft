var Cement = require('../Cement');
var assert = require('assert');

describe('CementTest', () => {
    describe('quand on passe une commande d`un sac de ciment', () => {
        it('Récupère un sac sans boite', () => {
            // when
            var cement = new Cement(1)
            //Then
            assert.deepEqual(cement.bags, 1)
        })
    })
    describe('quand on passe une commande 5 sacs de ciment', () => {
        it('Récupère 1 sac et une boite', () => {
            // when
            var cement = new Cement(5)
            //Then
            assert.deepEqual(cement.bags, 1)
            assert.deepEqual(cement.box, 1)
        })
    })
    describe('quand on passe une commande 6 sacs de ciment', () => {
        it('Récupère 2 sacs et une boite', () => {
            // when
            var cement = new Cement(6)
            //Then
            assert.deepEqual(cement.bags, 2)
            assert.deepEqual(cement.box, 1)
        })
    })
})