var assert = require('assert');
var CLI = require('../CLI.js');
var stdout = require("test-console").stdout;
var sinon = require('sinon');
sinon.stub(process, 'exit');

describe('CLITest', function () {

    describe('main', function () {
        it('Quit works', function () {

            var output = stdout.inspectSync(function () {
                // Given
                var prompt = () => 'quit';

                // Don't forget to decomment the line on CLI.js!!!!!
                // If not decomment, the test won't work!!!!!
                dacli = new CLI(prompt);

                // When
                dacli.main();
            });

            // Then
            assert.deepEqual(
                output,
                [
                    "\n--------------------------------------------------------------------------------\n",
                    "          Welcome to Efficent Command System 2.0\n",
                    "--------------------------------------------------------------------------------\n\n",
                    "\n--------------------------------------------------------------------------------\n",
                    "          System stopped\n",
                    "--------------------------------------------------------------------------------\n\n"
                ]
            );

        });
        it('Shows Help', function () {

            var output = stdout.inspectSync(function () {
                // Given
                var i = 0;
                var prompt = () => {
                    i++;
                    return i > 1 ? 'quit' : 'help';
                };

                // Don't forget to decomment the line on CLI.js!!!!!
                // If not decomment, the test won't work!!!!!
                dacli = new CLI(prompt);

                // When
                dacli.main();
            });

            // Then
            assert.deepEqual(
                output,
                [
                    "\n--------------------------------------------------------------------------------\n",
                    "          Welcome to Efficent Command System 2.0\n",
                    "--------------------------------------------------------------------------------\n\n",
                    "\nUnknown command\nThe available commands are the following : \n\n",
                    "quit - quit the program\n",
                    "order - create a new command\n",
                    "help - displays the help\n",
                    "\n",
                    "\n--------------------------------------------------------------------------------\n",
                    "          System stopped\n",
                    "--------------------------------------------------------------------------------\n\n"
                ]
            );

        });

        it('Quand on veut Order sans rajouter de commande', function () {

            var output = stdout.inspectSync(function () {
                // Given
                var i = 0;
                var prompt = () => {
                    i++;
                    switch (i) {
                        case 1:
                            return 'order'
                        case 2:
                            return 'stop'
                        case 3:
                            return 'stop'
                        case 4:
                            return 'quit'
                    }
                };

                commandLine = new CLI(prompt);

                // When
                commandLine.main();
            });

            // Then
            assert.deepEqual(
                output,
                [
                    "\n--------------------------------------------------------------------------------\n"
                    , "          Welcome to Efficent Command System 2.0\n"
                    , "--------------------------------------------------------------------------------\n\n"
                    , "\n--------------------------------------------------------------------------------\n"
                    , "          Order Menu\n"
                    , "--------------------------------------------------------------------------------\n\n"
                    , "New order created.\n"
                    , "Add new elements to your order\n"
                    , "\nNothing added\n"
                    , "\nQuit Order Menu\n\n"
                    , "\n--------------------------------------------------------------------------------\n"
                    , "          System stopped\n"
                    , "--------------------------------------------------------------------------------\n\n"
                ]
            );

        });

        it('Quand on veut Order et rajouter une commande', function () {

            var output = stdout.inspectSync(function () {
                // Given
                var i = 0;
                var prompt = () => {
                    i++;
                    switch (i) {
                        case 1:
                            return 'order'
                        case 2:
                            return 'prepend'
                        case 3:
                            return '50'
                        case 4:
                            return 'stop'
                        case 5:
                            return 'quit'
                    }
                };

                commandLine = new CLI(prompt);

                // When
                commandLine.main();
            });

            // Then
            assert.deepEqual(
                output,
                [
                    "\n--------------------------------------------------------------------------------\n",
                    "          Welcome to Efficent Command System 2.0\n",
                    "--------------------------------------------------------------------------------\n\n",
                    "\n--------------------------------------------------------------------------------\n",
                    "          Order Menu\n",
                    "--------------------------------------------------------------------------------\n\n",
                    "New order created.\n",
                    "Add new elements to your order\n",
                    "\nHow many perpends palets do you need ?\n",
                    "\nQuit Order Menu\n\n",
                    "\n--------------------------------------------------------------------------------\n",
                    "          System stopped\n",
                    "--------------------------------------------------------------------------------\n\n"
                ]
            );

        });
    });
});
